# TCR Party Contracts

## Initialize
The only environmental dependency you need is Node. Presently we can guarantee this all works with Node 8.
```
npm install
npm run compile
```

## Composition of the repo
The repo is composed as a Truffle project, and is largely idiomatic to Truffle's conventions. The tests are in the `test` directory, the contracts are in the `contracts` directory and the migrations (deployment scripts) are in the `migrations` directory.

## Deploying your own local TCR Party contracts
1. After cloning the repository locally, make sure all the dependencies are installed:
```
yarn
```
2. For your local environment you can download and open Ganache, or run your own local Ethereum client.
3. If you're using Ganache, set the environment variable MNEMONIC to the mnemonic exposed by Ganace.
```
export MNEMONIC="candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"
```
4. If you are using your own Ethereum client, open ./truffle.js and ensure that the 'development' network is configured for the correct port. By default it is set to http://localhost:7545 for Ganache.
5. Deploy the contracts to your local environment using the following commander
```
yarn deploy-development
```