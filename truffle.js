const HDWalletProvider = require('truffle-hdwallet-provider');
const fs = require('fs');

let mnemonic = '';
let infuraApiKey = '';
let privateKey = '';

if (fs.existsSync('secrets.json')) {
  const secrets = JSON.parse(fs.readFileSync('secrets.json', 'utf8'));
  ({ mnemonic, infuraApiKey, privateKey } = secrets);
}

if (process.env.MNEMONIC) {
  mnemonic = process.env.MNEMONIC;
}

module.exports = {
  networks: {
    development: {
      provider: () => new HDWalletProvider(mnemonic, 'http://localhost:7545'),
      network_id: '*',
      gas: 6000000,
      gasPrice: 25000000000,
    },

    // This is where the main contracts live!
    ropsten: {
      provider: () => new HDWalletProvider([privateKey], `https://ropsten.infura.io/v3/${infuraApiKey}`, 0, 1),
      network_id: '*',
      gas: 4500000,
      gasPrice: 25000000000,
    },

    rinkeby: {
      provider: () => new HDWalletProvider([privateKey], `https://rinkeby.infura.io/v3/${infuraApiKey}`, 0, 1),
      network_id: '*',
      gas: 4500000,
      gasPrice: 25000000000,
    },

    // config for solidity-coverage
    coverage: {
      host: 'localhost',
      network_id: '*',
      port: 7545, // <-- If you change this, also set the port option in .solcover.js.
      gas: 0xfffffffffff, // <-- Use this high gas value
      gasPrice: 0x01, // <-- Use this low gas price
    },
  },
};

